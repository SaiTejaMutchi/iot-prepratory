# Problem 1

### Goal : Write code to parse a CSV file and store the values into a key-value data structure and implement a search function for the data structure.

### Description

Write code to parse a CSV file named 'data.csv' and store the values into a 
key-value data structure made using structures and implement a search function 
for the data structure which will search through the data structure when given
`timestamp` value.

### CSV file Details

The CSV file named `data.csv` contains 4 columns, `sample`, `MLII`, `V5` & `timestamp`
and it contains 1000 data points with first line containing the headers.

Snippet of the CSV file:

| sample | MLII | V5   | timestamp           |
|--------|------|------|---------------------|
| 0      | 995  | 1011 | 1589422548019500000 |
| 1      | 995  | 1011 | 1589422548119500000 |
| 2      | 995  | 1011 | 1589422548219500000 |
| 3      | 995  | 1011 | 1589422548319510000 |
| 4      | 995  | 1011 | 1589422548419510000 |
| 5      | 995  | 1011 | 1589422548519510000 |
| 6      | 995  | 1011 | 1589422548619510000 |
| 7      | 995  | 1011 | 1589422548719520000 |
| 8      | 1000 | 1008 | 1589422548819520000 |
| 9      | 997  | 1008 | 1589422548919520000 |
| 10     | 995  | 1007 | 1589422549019530000 |

### What your program must do

1. **Parse a CSV file to get individual values from the CSV file**
2. **Store the CSV file in a key-value data structure with `sample` column being the key**
3. **Write a search function which then searches the key-value data structure when 
the timestamp is given as input and outputs the `sample`, `MLII`, `V5` for the given 
timestamp**


### For Example: 

Lets say 

#### Given (Input)
the timestamp input is given on the `stdin` when the program prompts the user
`What timestamp you want to search for:`
```sh
What timestamp you want to search for: 1589422549019530000
```

#### Then (output)
the program should output 
```
sample: 0      | MLII: 995  | V5: 1011 |
```

### Acceptance Criteria 
This is the criteria to accept the assignment.
- The program must do all that is given in the description above
- The code compiles and contains no syntax errors
- The code must be divided into functions 
- **Must** have documentation for using the program.
